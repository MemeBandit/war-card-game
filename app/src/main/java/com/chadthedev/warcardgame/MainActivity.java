package com.chadthedev.warcardgame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.Random;

public class MainActivity extends AppCompatActivity {


    ImageView iv_card_left, iv_card_right;
    TextView tv_score_left, tv_score_right;
    Button b_deal;

    Random r;

    int leftScore = 0, rightScore = 0;

    private TextView mTextMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("MainActivity", "01.on Create");



        iv_card_left = (ImageView) findViewById(R.id.iv_card_left);
        iv_card_right = (ImageView) findViewById(R.id.iv_card_right);
        tv_score_left = (TextView) findViewById(R.id.tv_score_left);
        tv_score_right = (TextView) findViewById(R.id.tv_score_right);
        b_deal = (Button) findViewById(R.id.b_deal);

        r = new Random();

        b_deal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //generate the two card numbers
                int leftCard = r.nextInt(13) + 1; //this is for cards 2 - 14
                int rightCard = r.nextInt(13) + 1; //this is for cards 2 - 14

                //show card images
                int leftImage = getResources().getIdentifier("card" + leftCard, "drawable", getPackageName());
                iv_card_left.setImageResource(leftImage);

                int rightImage = getResources().getIdentifier("card" + rightCard, "drawable", getPackageName());
                iv_card_right.setImageResource(rightImage);

                //compare cards, add points and display them
                if (leftCard > rightCard) {
                    leftScore++;
                    tv_score_left.setText(String.valueOf(leftScore));
                } else if (leftCard < rightCard) {
                    rightScore++;
                    tv_score_right.setText(String.valueOf(rightScore));
                } else {
                    Toast.makeText(MainActivity.this, "WAR", Toast.LENGTH_SHORT).show();
                }

                if(leftScore == 20){
                    Toast.makeText(MainActivity.this, "Player 1 won!", Toast.LENGTH_SHORT).show();
                    //tv_score_left.setText("0");
                    //tv_score_right.setText("0");
                    startActivity(new Intent(MainActivity.this, P1winscreen.class));
                } else if (rightScore == 20){
                    //tv_score_left.setText("0");
                    //tv_score_right.setText("0");
                    startActivity(new Intent(MainActivity.this, P2winscreen.class));
                }
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putString("my_text", tv_score_left.getText().toString());
        //outState.putString("my_text3", leftScore.getInt());
        outState.putString("my_text2", tv_score_right.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);

        tv_score_left.setText(savedInstanceState.getString("my_text"));
        tv_score_right.setText(savedInstanceState.getString("my_text2"));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("MainActivity", "02.on Start");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("MainActivity", "03.on Resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("MainActivity", "04.on Pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("MainActivity", "05.on Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("MainActivity", "06.on Destroy");
    }

}


