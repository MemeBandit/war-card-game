package com.chadthedev.warcardgame;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class Account extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        //Head
        //Layout type
        final RelativeLayout myLayout = new RelativeLayout(this);

        //Original Background color
        myLayout.setBackgroundColor(Color.WHITE);


//Body

        //Button
        final Button Changer_Button = new Button(this);

        //Button Properties
        Changer_Button.setText("Click Here to change theme");
        Changer_Button.setBackgroundColor(Color.LTGRAY);

        //Position Properties
        RelativeLayout.LayoutParams ChangerButton_Position_Details =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT
                );
        ChangerButton_Position_Details.addRule(RelativeLayout.CENTER_HORIZONTAL);
        ChangerButton_Position_Details.addRule(RelativeLayout.CENTER_VERTICAL);

        myLayout.addView(Changer_Button,ChangerButton_Position_Details);

        //On click listener
        Changer_Button.setOnClickListener(
                new Button.OnClickListener() {

                    public void onClick(View v) {

                        Changer_Button.setText("Button Clicked");
                        myLayout.setBackgroundColor(Color.DKGRAY);
                    }
                }
        );

        setContentView(myLayout);
    }
}

