package com.chadthedev.warcardgame;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;


public class MainMenu extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;


    private static final int MY_REQUEST_CODE = 7117; //Entry code
    List<AuthUI.IdpConfig> providers;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Log.i("MainMenu", "01.on Create");


        //list¬
        ListFragment fragment = new ListFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //fragmentTransaction.add(R.id.drawer_layout, fragment);
        fragmentTransaction.commit();



        //Init provider
        providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(), // Email Builder
                new AuthUI.IdpConfig.PhoneBuilder().build(), // Phone Builder
                new AuthUI.IdpConfig.FacebookBuilder().build(), // Facebook Builder
                new AuthUI.IdpConfig.GoogleBuilder().build() // Google Builder
        );

        showSignInOptions(

        );


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new GameFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_playgame);
        }


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_playgame:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new GameFragment()).commit();


                break;
            case R.id.nav_account:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new AccountFragment()).commit();
                break;
            case R.id.nav_artwork:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ArtworkFragment()).commit();
                break;
            case R.id.nav_exit:
                Toast.makeText(this, "Exiting app...", Toast.LENGTH_SHORT).show();
                System.exit(0);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }











/*
        Button myButton1 = (Button) findViewById(R.id.btnPlay);

        myButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainMenu.this, MainActivity.class));
            }
        });

        Button myButton2 = (Button) findViewById(R.id.btnAccount);

        myButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainMenu.this, Account.class));
            }
        });

        Button myButton3 = (Button) findViewById(R.id.btnQuit);

        myButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.exit(0);
            }
        });  */
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("MainMenu", "02.on Start");

        /*new Date();
        Time currentTime = new Time();
        currentTime.setToNow();*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("MainMenu", "03.on Resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("MainMenu", "04.on Pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("MainMenu", "05.on Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("MainMenu", "06.on Destroy");
    }

    private void showSignInOptions() {
        startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setTheme(R.style.MyTheme)
                        .build(), MY_REQUEST_CODE
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                //GetUser
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                //get email on toast
                Toast.makeText(this, "" + user.getEmail(), Toast.LENGTH_SHORT).show();


            } else {//open else
                Toast.makeText(this, "" + response.getError().getMessage(), Toast.LENGTH_SHORT).show();

            }//close else
        }
    }
}
