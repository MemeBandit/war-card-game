package com.chadthedev.warcardgame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class PinLock extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_lock);

        //Pinlock
        final String password = "1234";
        Pinview pinview = (Pinview) findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                //API CALLS OR NEXT ACTIONS//
                String tempPassword = pinview.getValue();

                if (tempPassword.equals(password)) {
                    Toast.makeText(PinLock.this, pinview.getValue(), Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(PinLock.this, MainMenu.class));
                } else {
                    Toast.makeText(PinLock.this, "Password is incorrect", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }
}
