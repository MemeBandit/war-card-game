package com.chadthedev.warcardgame;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class AccountFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


        final Button myButton1 = (Button) getView().findViewById(R.id.btnColorOriginal);

        myButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                myButton1.setBackgroundColor(Color.RED);


            }
        });

        final Button myButton2 = (Button) getView().findViewById(R.id.btnColorDark);

        myButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                myButton2.setBackgroundColor(Color.DKGRAY);


            }
        });

    }
}
