package com.chadthedev.warcardgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class P2winscreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p2winscreen);


        Button myButton1 = (Button) findViewById(R.id.btnReplay1);

        myButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(P2winscreen.this, MainActivity.class));
            }
        });

        Button myButton2 = (Button) findViewById(R.id.btnBackToMenu);

        myButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(P2winscreen.this, PinLock.class));
            }
        });
    }
}